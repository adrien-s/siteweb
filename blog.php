<?php
	require_once("inc/parsedown.php");
	require_once("inc/perms.php");
	require_once("inc/db.php");
	require_once("inc/utils.php");
	require_once("inc/tags.php");
	require_once("inc/taglist.php");

	/* Gets action to process */
	if (isset($_GET['do'])) {
		$action=$_GET['do'];
	} else {
		$action="montharticles";
	}

	/* Opens the database */
	$blogdb = new storage("blog");
	$billets = $blogdb->getDB();

	/* Create menu with all months from a specific year */
	for($i=0;$i<=17;$i++) {
		$activelist[$i] = '';
	}
	$month=['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
	$curmonth=date('n')-1;
	if (isset($_GET['month'])) {
		$curmonth=$_GET['month'];
		$bpage=1+$curmonth;
	} else if (isset($_GET['bpage'])) {
		$bpage=$_GET['bpage'];
	} else {
		$bpage=1+$curmonth;
	}
	if (isset($_GET['year'])) {
		$curyear=$_GET['year'];
	} else {
		$curyear=date('Y');
	}

	/* Get selector yyyy-mm for articles */
	if ($curmonth<9) {
		$month_selector=$curyear."-0".($curmonth+1);
	} else {
		$month_selector=$curyear."-".($curmonth+1);
	}

	$nextyear=$curyear+1;
	$prevyear=$curyear-1;
	$activelist[$bpage]=' class="active"';
	if ($action == "del") {
		header("Location: blog.php?month=".$curmonth."&amp;year=".$curyear);
	}

	if ($action!="rss") {
?>
<!DOCTYPE html>
<html>
<?php $page=2; include('header.php'); ?>
		<div class="container-fluid">
			<div class="row-fluid">
				<!-- Menu -->
				<div class="span2">
					<div class="well sidebar-nav" id="menuleft">
						<ul class="nav nav-list menu-lateral">
							<li class="nav-header text-center">Articles</li>
							<li class="text-center"><b><a href="?year=<?php echo $prevyear; ?>&amp;month=11"<?php echo $activelist[0]; ?>><?php echo $prevyear; ?></a></b></li>
							<?php for($i=1;$i<13;$i++) { ?>
							<li class="text-center"><a href="?month=<?php echo ($i-1); ?>&amp;year=<?php echo $curyear; ?>" <?php echo $activelist[$i]; ?>><?php echo $month[$i-1]; ?></a></li>
							<?php } ?>
							<li class="text-center"><b><a href="?year=<?php echo $nextyear; ?>&amp;month=0"<?php echo $activelist[13]; ?>><?php echo $nextyear; ?></a></b></li>
							<hr />
							<li class="nav-header text-center">Rechercher</li>
							<li><a href="?bpage=14"<?php echo $activelist[14]; ?>>Par tag</a></li>
							<li><a href="?bpage=15"<?php echo $activelist[15]; ?>>Par contenu</a></li>
<?php if (true || isset($_SESSION['login'])) { ?>
							<hr />
							<li><a href="?bpage=16"<?php echo $activelist[16]; ?>>Ajouter un article</a></li>
							<li><a href="?bpage=17"<?php echo $activelist[17]; ?>>Liste des tags</a></li>
<?php } ?>
						</ul>
					</div>
				</div>

				<!-- Articles / Contenu -->
				<div class="span9">
<?php
	/* If a hash has been given, display this only article, else display all articles from the current selected month. */
	if (isset($_GET['hash'])) {
		$cur_hash = $_GET['hash'];
		if ($action == "edit") {
			edit_article($cur_hash);
		} else if ($action == "save") {
			saved_article();
		} else if ($action == "del") {
			del_article($cur_hash);
		} else if (isset($billets[$cur_hash]) && $billets[$cur_hash]!=="") {
			base_article($cur_hash, $billets[$cur_hash]['title'], $billets[$cur_hash]['author'], $billets[$cur_hash]['date'], $billets[$cur_hash]['text'], false);
		}
	}
	if ((!isset($_GET['hash'])) || $action == "del") {
		if ($bpage>0 && $bpage<14 && ($action=="montharticles" || $action == "del")) {
			read_month_articles();
		}
		if ($bpage == 14) {
			search_tag_ui();
		}
		if ($bpage == 15) {
			search_content_ui();
		}
		if ($bpage == 16) {
			write_article();
		}
		if ($bpage == 17) {
			tag_list_ui();
		}
	}
?>
				</div>
			</div>
		</div>
<?php include('footer.php'); ?>
</html>
<?php }
	/** Display RSS data **/
	else {
		generate_rss();
	}

	/***************************************** Pages ********************************************/
	/******************** Articles reading ********************/
	/** Article displayer **/
	function base_article($id, $title, $author, $timestamp, $content, $collapsed=true)
	{
		global $curmonth, $curyear, $billets;
		if ($collapsed) {
			$collapse_class="collapse";
		} else {
			$collapse_class="";
		}
		if (canReadThat($billets[$id]['auth'], false)) {
?>
	<article>
		<div class="article-header">
			<p><?php if ($collapsed) { ?><a href="#" title="Dérouler l'article" class="btn" data-toggle="collapse" data-target="#art<?php echo $id; ?>"><b class="caret"></b></a><?php } ?><a title="Lire cet article en pleine page" class="article-title" href="blog.php?hash=<?php echo $id; ?>&amp;month=<?php echo $curmonth; ?>&amp;year=<?php echo $curyear; ?>"><strong><?php echo $title; ?></strong></a> <small><em>Par <?php echo $author; ?>, le <?php echo formatMaDate($timestamp); ?></em></small></p>
		</div>
		<div class="article-body <?php echo $collapse_class; ?>" id="art<?php echo $id; ?>">
			<p>Tags : <?php tagLst($billets[$id]['tags']); ?></p>
			<hr />
<?php
		echo parse($content);
		echo "<hr />";
		if (canReadThat($billets[$id]['auth_mod'], false)) {
			echo "<a title='Ça ne vous plaît pas ? Modifiez !' href='blog.php?do=edit&amp;hash=".$id."'>Modifier</a><br />";
		}
		if (allowDel($billets[$id]['author'])) {
			echo "<a title='Jeter cet article dans les profondeurs ténébreuses et pleines de microbes de l&#39;oubli ?' href='blog.php?do=del&amp;hash=".$id."&amp;month=".$curmonth."&amp;year=".$curyear."'>Supprimer</a>";
		}
?>
		</div>
	</article>
<?php
		}
	}

	/** Read all month's articles at once **/
	function read_month_articles()
	{
		global $billets, $month_selector;
		foreach(array_reverse($billets) as $billet) {
			/* Check the article is readable */
			if ($billet['hash']!="" && (strpos($billet['date'], $month_selector)!==false) && canReadThat($billet['auth'], false) ) {
				base_article($billet['hash'], $billet['title'], $billet['author'], $billet['date'], $billet['text']);
			}
		}
	}

	/******************** Write articles ********************/
	/** Article writer form **/
	function write_article()
	{
		$artHash=md5(date('U'));
		form_article_edit($artHash);
	}

	/** Edit an article */
	function edit_article($id)
	{
		global $billets;
		form_article_edit($id, $billets[$id]['title'], $billets[$id]['text'], $billets[$id]['auth'], $billets[$id]['auth_mod'], $billets[$id]['tags']);
	}

	/** Edit/Create form **/
	function form_article_edit($id, $title="", $content="", $auth="default rss", $auth_mod="", $tags="tag1 tag2")
	{
		$rights = verPerm("blog/billet");
		if ($rights['add']=="no") {
?>
	<div class="container">
		<div class="row-fluid">
			<div class="span3"></div>
			<div class="span6 block text-center">
				<p>Désolé, vous n'avez pas la permission de faire cela.</p>
			</div>
		</div>
	</div>
<?php
		} else {
			if ($auth_mod == "") {
				$auth_mod = $_SESSION['login'];
			}
?>
	<form method="POST" action="blog.php?do=save&amp;hash=<?php echo $id; ?>">
		<input class="span12" type="text" name="title" value="<?php echo $title; ?>" placeholder="Titre"/><br />
		<textarea class="span12 fixedMaxWidth" style="min-height: 300px;" name="text" placeholder="Texte du billet (HTML5 ou MarkDown)"><?php echo $content; ?></textarea><br />

		<div class="row-fluid">
			<span class="span6">Lecture (default, rss, un login, avec ou sans !)
			<input class="span12" type="text" name="auth" value="<?php echo $auth; ?>" placeholder="Lecture" /></span>

			<span class="span6">Ecriture (default, rss, un login, avec ou sans !)
			<input class="span12" type="text" name="auth_mod" value="<?php echo $auth_mod; ?>" placeholder="Ecriture" /></span>
		</div>
		<span class="span12">Tags :</span>
		<input class="span12" type="text" name="tags" value="<?php echo $tags; ?>" placeholder="Tags" />

		<input class="btn pull-right" type="submit" value="Écrire !" />
	</form>
<?php
		}
	}

	/** Article saved **/
	function saved_article()
	{
		global $blogdb;
		$id = $_GET['hash'];
		$author = $_SESSION['login'];
		$timestamp = date('Y-m-d H:i:s');
		if (isset($billets[$id]) && $billets[$id]['hash']!=="") {
			del_article($id);
		}
		$blogdb->updateBuffer();
		$blogdb->addEntry($id, 'hash', $id);
		$blogdb->addEntry($id, 'author', $_SESSION['login']);
		$blogdb->addEntry($id, 'date', $timestamp);
		$blogdb->addEntry($id, 'title', $_POST['title']);
		$blogdb->addEntry($id, 'text', $_POST['text']);
		$blogdb->addEntry($id, 'auth', $_POST['auth']);
		$blogdb->addEntry($id, 'auth_mod', $_POST['auth_mod']);
		$blogdb->addEntry($id, 'tags', $_POST['tags']);
		$blogdb->sync();

		if (canReadThat($_POST['auth'], true)) {
			add_rss_entry($id, $author, $timestamp, $_POST['title'], $_POST['text'], $_POST['tags']);
		}
?>
	<div class="container">
		<div class="row-fluid">
			<div class="span3"></div>
			<div class="span6 block text-center">
				<p>L'article a bien été enregistré.</p>
				<p><a href="blog.php?hash=<?php echo $id; ?>">Aller le lire</a></p>
			</div>
		</div>
	</div>
<?php
	}

	/** Deletes an article **/
	function del_article($id)
	{
		global $blogdb;
		$blogdb->updateBuffer();
		$blogdb->deleteEntry($id);
		$blogdb->sync();
	}

	/******************** Search ********************/
	/** Tag list **/
	function tag_list_ui()
	{
		global $billets;
		$tag_engine = new tagList($billets);
?>
	<div class="container">
		<div class="row-fluid">
			<div class="span3"></div>
			<div class="span6 block">
				<p>Liste des tags utilisés :</p>
				<?php $tag_engine->showTags(); ?>
			</div>
		</div>
	</div>
<?php
	}
	/** Tag Search **/
	function search_tag_ui()
	{
		global $billets;

		$search_data = array();
		if (isset($_REQUEST['tags'])) {
			$search_data = search_tag_run($_REQUEST['tags']);
		}
		$data_found = count($search_data);
?>
	<div class="container">
		<div class="row-fluid">
			<div class="span3"></div>
			<form class="span6 block" id="search-form" action="blog.php?bpage=14" method="POST">
				<p>Rechercher les tags suivants :</p>
				<input class="span12" type="text" placeholder="Tags à chercher" name="tags" value="<?php if (isset($_REQUEST['tags'])) { echo $_REQUEST['tags']; } else { echo "tag1 tag2"; } ?>" title="Entrez quelques tags à chercher" />
				<div class="span9"></div><input class="span3 btn" type="submit" name="submit" value="Chercher" title="Lancer la recherche par tags" />
			</form>
		</div>
	</div>
<?php
		if ($data_found>0) {
			echo "<h4>Résultats<hr /></h4>";
			foreach($billets as $billet) {
				foreach($search_data as $tag_to_search) {
					if ($billet['hash']!='' && strpos(' '.$billet['tags'].' ', ' '.$tag_to_search.' ')!==false) {
						base_article($billet['hash'], $billet['title'], $billet['author'], $billet['date'], $billet['text']);
					}
				}
			}
		}
	}

	/** Tag search engine **/
	function search_tag_run($tags)
	{
		$taglist = explode(' ', $tags);
		return $taglist;
	}

	/** Content Search **/
	function search_content_ui()
	{
		global $billets;

		$search_data = array();
		if (isset($_REQUEST['words'])) {
			$search_data = search_content_run($_REQUEST['words']);
		}
		$data_found = count($search_data);
?>
	<div class="container">
		<div class="row-fluid">
			<div class="span3"></div>
			<form class="span6 block" id="search-form" action="blog.php?bpage=15" method="POST">
				<p>Rechercher les termes suivants :</p>
				<input class="span12" type="text" placeholder="Texte à chercher" name="words" value="<?php if (isset($_REQUEST['words'])) { echo $_REQUEST['words']; } else { echo "mots"; } ?>" title="Entrez quelques mots à chercher" />
				<div class="span9"></div><input class="span3 btn" type="submit" name="submit" value="Chercher" title="Lancer la recherche par mots" />
			</form>
		</div>
	</div>
<?php
		if ($data_found>0) {
			echo "<h4>Résultats<hr /></h4>";
			foreach($billets as $billet) {
				foreach($search_data as $word_to_search) {
					if ($billet['hash']!='' && strpos($billet['text'], $word_to_search)!==false) {
						base_article($billet['hash'], $billet['title'], $billet['author'], $billet['date'], $billet['text']);
					}
				}
			}
		}
	}

	/** Content Search engine **/
	function search_content_run($words)
	{
		$wordlist = explode(' ', $words);
		return $wordlist;
	}

	/******************** RSS data ********************/
	/** Generates the RSS data **/
	function generate_rss()
	{
		global $billets;
		// Search the RSS file
		if (file_exists("rss.xml")) {
			$rss_file = fopen("rss.xml", "r");
			$rss_data = fread($rss_file, filesize("rss.xml"));
			echo $rss_data;
			fclose($rss_file);
		} else {
			// If it doesn't exist, first generates it
			// header
			$rss_file = fopen("rss.xml", "w");
			$head='<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Blog</title>
		<description>Flux RSS du blog</description>
		<lastBuildDate>'.date('r').'</lastBuildDate>
		<link rel="self">http://www.art-software.fr/blog.php?do=rss</link>
	</channel>
</rss>';
			echo $head;
			fwrite($rss_file, $head);
			fclose($rss_file);
		}
	}

	/** Adds an entry to the RSS file **/
	function add_rss_entry($id, $author, $date, $title, $text, $tags)
	{
		if (!file_exists("rss.xml")) {
			$rss_file = fopen("rss.xml", "w");
			$head='<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Blog</title>
		<description>Flux RSS du blog</description>
		<lastBuildDate>'.date('r').'</lastBuildDate>
		<link rel="self">http://www.art-software.fr/blog.php?do=rss</link>
	</channel>
</rss>';
			fwrite($rss_file, $head);
			fclose($rss_file);
		}
		$d = strptime($date.' +0100', '%Y-%m-%d %H:%M:%S %z');

		$rss_xml = 	simplexml_load_file("rss.xml");
		while ($rss_xml === FALSE) {
			usleep(1000);
			$rss_xml = 	simplexml_load_file("rss.xml");
		}
		$chan = $rss_xml->xpath("/rss/channel");
		$new_item = $chan[0]->addChild("item");
		$new_item->addChild("author", $author);
		$new_item->addChild("title", $title);
		$new_item->addChild("description", parse($text));
		$new_item->addChild("pubdate", date("r", mktime($d['tm_hour'], $d['tm_min'], $d['tm_sec'], $d['tm_mon']+1, $d['tm_mday'], 1900+$d['tm_year'])));
		$permalink = $new_item->addChild("guid", $id);
		$permalink->addAttribute("isPermalink", "true");
		$new_item->addChild("link", "http://www.art-software.fr/blog.php?hash=".$id);
		$rss_xml->asXml("rss.xml");
	}
?>
