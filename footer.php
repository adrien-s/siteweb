<!-- Footer -->
	<div class="container">
		<hr />
		<footer class="text-center">
			<p>Copyright &copy; 2013-2017, Art SoftWare − License Creative Commons <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="img/ccbysa.png" /></a> − Contact: <a href="mailto:admin@art-software.fr">support@art-software.fr</a></p>
		</footer>
	</div>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.js"></script>
</body>
