<?php
class storage {
	private $data;
	private $path;

	public function __construct($dbname) {
		$this->path = 'data/'.$dbname.'.db';
		$file_data = fopen($this->path, 'r');
		if ($file_data!=FALSE) {
			if (filesize($this->path)>0) {
				$this->data = unserialize(gzinflate(fread($file_data, filesize($this->path))));
			}
			fclose($file_data);
		}
	}
	public function __destruct() {
	}

	// Get the database
	public function &getDB() {
		return $this->data;
	}

	// Updates the buffer
	public function updateBuffer() {
		$file_data = fopen($this->path, 'r');
		if ($file_data!=FALSE) {
			if (filesize($this->path)>0) {
				$this->data = unserialize(gzinflate(fread($file_data, filesize($this->path))));
			}
			fclose($file_data);
		}
	}

	// Add an entry
	public function addEntry($key, $field, $value) {
		$this->data[$key][$field] = $value;
	}
	// Reads an entry
	public function getEntry($key, $field) {
		return $this->data[$key][$field];
	}
	// Deletes an entry
	public function deleteEntry($key) {
		unset($this->data[$key]);
	}
	// Sync the database to filesystem
	public function sync() {
		$file_name = fopen($this->path, 'w');
		fwrite($file_name, gzdeflate(serialize($this->data)));
		fclose($file_name);
	}
}
?>
