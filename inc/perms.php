<?php
	function verPerm($node) {
		libxml_disable_entity_loader(false);
		$xml = simplexml_load_file('data/users.xml');
		if(isset($_SESSION["login"])) {
			$rights = $xml->xpath('/conf/'.$node.'/'.$_SESSION['login']);
			if(! isset($rights[0])) {
				$rights = $xml->xpath('/conf/'.$node.'/registered');
			}
		} else {
			$rights = $xml->xpath('/conf/'.$node.'/default');
		}
		$rights = $rights[0];
		return $rights;
	}
	function canReadThat($permlist, $isRSS) {
		$canread = false;
		$perms=' '.$permlist.' ';
		if (isset($_SESSION['login']) && $_SESSION['login']=="root")
			return true;
		if (strpos($perms,' default ')!==false)
			$canread=true;
		if (strpos($perms,' !registered ')!==false)
			$canread=(! isset($_SESSION['login']));
		if (strpos($perms,' registered ')!==false)
			$canread=isset($_SESSION['login']);
		if (isset($_SESSION['login']) && strpos($perms,' '.$_SESSION['login'].' ')!==false)
			$canread=true;
		if (isset($_SESSION['login']) && strpos($perms,' !'.$_SESSION['login'].' ')!==false)
			$canread=false;
		if ($isRSS) {
			$canread=false;
			if (strpos($perms,' rss ')!==false) {
				$canread=true;
			}
		}

		return $canread;
	}
	function allowDel($prop) {
		$rights = verPerm("blog/billet");
		if ( (isset($_SESSION['login']) && $rights['del']=="own" && $prop==$_SESSION['login']) ||
			($rights['del']=="all"))
			return true;
		return false;
	}
?>
