<?php
function formatMaDate($dateOrg) {
	$mois=array ('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
	return substr($dateOrg, 8, 2).' '.$mois[substr($dateOrg,5,2)-1].' '.substr($dateOrg,0,4).' '.substr($dateOrg,11,8);
}
?>
