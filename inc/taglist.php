<?php
class tagList
{
	private $_posts;
	private $_tagArray;

	/**
	 * Constructor
	 */
	public function __construct($postsArray)
	{
		if (isset($postsArray)) {
			$this->_posts = $postsArray;
			$this->updateData();
		}
	}

	/**
	 * Update posts array
	 * @param $posts New posts array
	 */
	public function setPostArray($posts)
	{
		if (isset($posts)) {
			$this->_posts = $posts;
		}
	}

	/**
	 * Computes tags occurences
	 */
	public function updateData()
	{
		$tagArray = array();
		if (isset($this->_posts)) {
			foreach($this->_posts as $post)
			{
				if (isset($post['hash']) && $post['hash']!=='') {
					$tagArray = explode(' ', $post['tags']);
					foreach($tagArray as $tag)
					{
						if (isset($this->_tagArray[$tag])) {
							$this->_tagArray[$tag]['nb']++;
						} else {
							$this->_tagArray[$tag]['nb'] = 1;
							$this->_tagArray[$tag]['name'] = $tag;
						}
					}
				}
			}
			ksort($this->_tagArray);
		}
	}

	/**
	 * Return full array
	 * @return Tag array
	 */
	public function getFullArray()
	{
		return $this->_tagArray;
	}

	/**
	 * Return quantity for a specified tag
	 * @param $tag The tag to get
	 * @return -1 if tag does not exist
	 *         the number of tag's occurences otherwise
	 */
	public function getQuantity($tag)
	{
		$nb = -1;
		if (isset($this->_tagArray) && isset($this->_tagArray[$tag]['nb'])) {
			$nb = $this->_tagArray[$tag]['nb'];
		}
		return $nb;
	}

	/**
	 * Displays the tag's list and link each one to the search UI
	 */
	public function showTags()
	{
		if (isset($this->_tagArray)) {
			foreach($this->_tagArray as $tag)
			{
				echo '<a href="blog.php?bpage=14&amp;tags='.$tag['name'].'"><span class="tag">'.$tag['name'].' ('.$tag['nb'].')</span></a>';
			}
		}
	}
}
?>
