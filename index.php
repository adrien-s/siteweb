<?php
	require_once("inc/parsedown.php");
	require_once("inc/db.php");
	require_once("inc/perms.php");

	$sitedb = new storage("site");
	$action = "read";
	if (isset($_GET["do"])) {
		$action=$_GET["do"];
	}
?>
<!DOCTYPE html>
<html>
<?php
	if ($action !== "save") {
		$page=1;
		include('header.php');
	}
	if ($action == "modify") {
		edito_set();
	} else if ($action == "save") {
		edito_save();
	} else {
		edito_show();
	}
	if ($action !== "save") {
		include('footer.php');
	}
?>
</html>

<?php
	/**************** Pages *********************/
	/* Default: show edito & rss */
	function edito_show()
	{
		global $sitedb;
?>
		<div class="container">
			<div class="row-fluid">
				<div class="span4">
					<div class="newslisttitle"><p>Dernières news</p></div>
					<div class="block newslist">
						<?php
							$blogdb = new storage("blog");
							$blogdb->updateBuffer();
							$billets = $blogdb->getDb();
							$max = 30;
							foreach(array_reverse($billets) as $billet) {
								if (canReadThat($billet["auth"], True)) {
									$max--;
									echo("
						<div class=\"newsitem\">
							<div class=\"newsitemhead\">
								<div class=\"newsitemtitle\">".$billet["title"]." <em>(<a href=\"blog.php?hash=".$billet["hash"]."&year=".substr($billet["date"], 0, 4)."&month=".(substr($billet["date"], 5, 2)-1)."\">permalink</a>)</em></div>
								<div class=\"newsitemauthor\">Par ".$billet["author"].", le ".$billet["date"]."</div>
							</div><div class=\"newsitemcontent\">".str_replace(['<', '>'], ['&lt;', '&gt;'], substr($billet["text"], 0, 64))."...</div>
						</div>");
								}
								if ($max<0) {
									break;
								}
							}
						?>
					</div>
					<div class="newslisttitle"><p>Blogs intéressants</p></div>
					<div class="block newslist">
						<div class="newsitem"> <!-- Le Buddablog -->
							<div class="newsitemhead">
								<div class="newsitemtitle"><a href="http://buddakhiin.blogspot.com">Le Buddablog</a></div>
							</div>
							<div class="newsitemcontent">
								<p>Blog d'un geek cafféinomane fan de W40k</p>
							</div>
						</div>
						<div class="newsitem"> <!-- Korben -->
							<div class="newsitemhead">
								<div class="newsitemtitle"><a href="http://korben.info">Korben.info</a></div>
							</div>
							<div class="newsitemcontent">
								<p>Le papa des Internets ; on ne le présente plus !</p>
							</div>
						</div>
						<div class="newsitem"> <!-- CommitStrip -->
							<div class="newsitemhead">
								<div class="newsitemtitle"><a href="http://www.commitstrip.com/fr">CommitStrip</a></div>
							</div>
							<div class="newsitemcontent">
								<p>Un blog tout en planches et en phylactères pour rire des mésaventures de développeur &amp; cie.</p>
							</div>
						</div>
						<div class="newsitem"> <!-- BronyCUB -->
							<div class="newsitemhead">
								<div class="newsitemtitle"><a href="http://www.bronycub.org">BronyCUB</a></div>
							</div>
							<div class="newsitemcontent">
								<p>Pas vraiment un blog, mais si vous aimez l'univers de <span title="My Little Pony : Friendship is Magic"><u>MLP:FiM</u></span> et êtes sur Bordeaux, venez nous voir !</p>
							</div>
						</div>
					</div>
				</div>
				<div class="span8 newslisttitle"><p>Éditorial</p></div>
				<div class="span8 greyblock" id="edito">
					<?php
							/* Getting edito data from db */
							$e_title=$sitedb->getEntry('edito', 'title');
							$e_text=parse(str_replace('&','&amp;',$sitedb->getEntry('edito', 'text')));
					?>
					<h3><?php echo $e_title; ?></h3>
					<?php echo $e_text; ?>
					<?php
						/* Can set edito */
						$rights = verPerm("edito");
						if ($rights['set']=="yes") {
					?>
					<hr />
					<a href="index.php?do=modify">Modifier</a>
					<?php
							}
					?>
				</div>
			</div>
		</div>
<?php
	}

	/* Set the Editorial */
	function edito_set()
	{
		global $sitedb;
		/* Récupération des données existantes */
		$title = $sitedb->getEntry('edito', 'title');
		$content = $sitedb->getEntry('edito', 'text');

		/* Formulaire de modification */
?>
	<div class="content">
		<div class="row-fluid">
			<div class="span2"></div>
				<form class="span8" method="POST" action="index.php?do=save">
					<input class="span12" type="text" name="edito_title" value="<?php echo $title; ?>" placeholder="Titre"/><br />
					<textarea class="span12 fixedMaxWidth" style="min-height: 300px;" name="edito_text" placeholder="Texte de l'édito (HTML5 ou MarkDown)"><?php echo $content; ?></textarea><br />
					<input class="btn pull-right" type="submit" value="Écrire !" />
				</form>
			</div>
		</div>
	</div>
<?php
	}

	/* Save the newly modified edito */
	function edito_save()
	{
		global $sitedb;
		$sitedb->updateBuffer();
		$sitedb->addEntry('edito', 'title', $_POST['edito_title']);
		$sitedb->addEntry('edito', 'text', $_POST['edito_text']);
		$sitedb->sync();
		header("Location: index.php");
	}
?>
