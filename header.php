<!-- Header -->
<head>
<?php
	require_once("inc/perms.php");
	for($i=0 ; $i<=6 ; $i++) {
		$listClass[$i] = '';
	}
	$listClass[$page]='class="active"';
	$title=[1 => 'Accueil du site',
		2 => 'Blog',
		3 => 'Shaarli',
		4 => 'Applications',
		5 => 'Roundcube',
		6 => 'A propos de moi'];
	if (!isset($session_started)) {
		session_start();
		$session_started=true;
	}
	if ($page !== 42 ) {
		$_SESSION['url'] = $_SERVER['REQUEST_URI'];
	}
?>
	<title><?php echo $title[$page]; ?> - Art SoftWare</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<link href="css/bootstrap.css" rel="stylesheet" />
		<link href="css/bootstrap_delta.css" rel="stylesheet" />
		<link href="css/bootstrap-responsive.css" rel="stylesheet" />
		<link href="css/bootstrap-responsive_delta.css" rel="stylesheet" />
		<link href="css/main.css" rel="stylesheet" />
		<link rel="alternate" type="application/rss+xml" href="http://www.art-software.fr/rss/site" title="Flux RSS du blog" />
		<link rel="alternate" type="application/rss+xml" href="http://www.art-software.fr/rss/shaarli" title="Flux RSS du Shaarli" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="ico/favicon.png">
	</head>
	<body>
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="#">Art SoftWare<img id="logo" alt="logo" src="img/logoHeader.png" /></a>
					<div class="nav-collapse collapse">
						<ul class="nav">
							<li <?php echo $listClass[1]; ?>><a title="Accueil du site" href="index.php">Accueil</a></li>
							<li <?php echo $listClass[2]; ?>><a title="Mon blog" href="blog.php">Blog</a></li>
							<li <?php echo $listClass[3]; ?>><a title="Le shaarli, plein de trucs potentiellement utiles… Ou pas :p" href="/links/">Liens</a></li>
							<li><a title="Le Wiki !" href="//wiki.art-software.fr/doku.php">Wiki</a></li>
							<li><a title="Partage de fichiers !" href="//files.art-software.fr/index.php">Fichiers</a></li>
							<li><a title="Mes projets" href="http://git.art-software.fr/explore/repos">Dépôt Git</a></li>
							<li><a title="Mon CV" href="/cv.png">CV</a></li>
						</ul>
						<ul class="nav pull-right">
<?php /********** Normal version of login **********/ ?>
							<li class="dropdown hideBelow980">
<?php
	if (isset($_SESSION['login'])) {
?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['login']; ?> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a title="Vous voulez déjà partir ? :(" href="connection.php?do=logout">Me déconnecter</a></li>
								</ul>
<?php
	} else {
?>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Connexion <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li>
										<form class="navbar-form text-center" id="connect-form" method="post" action="connection.php?do=login">
											<input class="span2" name="login" type="text" placeholder="Login" />
											<input class="span2" name="password" type="password" placeholder="Mot de passe" />
											<button type="submit" class="btn">connecter !</button>
										</form>
									</li>
								</ul>
<?php
	}
?>
							</li>
						</ul>
<?php /*********** Small version of login *********/ ?>
							<li class="dropdown hideAbove980">
<?php if (isset($_SESSION['login'])) { ?>
								<p id="userName"><center><?php echo $_SESSION['login']; ?></center></p>
								<div id="topOfConnectedMenu">
<?php } ?>
									<ul class="nav">
<?php
	if (isset($_SESSION['login'])) {
?>
										<li><a title="Vous voulez déjà partir ? :(" href="connection.php?do=logout">Me déconnecter</a></li>
									</ul>
<?php if (isset($_SESSION['login'])) { ?>
								</div>
<?php
		}
	} else {
?>
								<ul class="nav">
									<li><center>Connexion</center>
										<form class="navbar-form text-center" id="connect-form" method="post" action="connection.php?do=login">
											<input class="span2" name="login" type="text" placeholder="Login" />
											<input class="span2" name="password" type="password" placeholder="Mot de passe" />
											<a title="Inscrivez-vous ! C'est gratuit ! (et non, je ne vends pas d'informations personnelles, je suis un particulier, pas une entreprise de merde)" href="connection.php?do=sign">Pas encore inscrit ?</a>
											<button type="submit" class="btn">connecter !</button>
										</form>
									</li>
								</ul>
<?php
	}
?>
							</li>
						</ul>

					</div>
				</div>
			</div>
		</div>
<!-- Body -->
