<?php
$action = "login";
if (isset($_GET["do"])) {
	$action = $_GET["do"];
}

/* Select action to process */
if ($action == "login" || $action == "logout") {
	connection_login($action);
} /* else if ($action == "sign") {
	$page=42;
	include("header.php");
	connection_signup();
} else if ($action == "register") {
	$page=42;
	include("header.php");
	connection_register_user();
} */

/******************** Pages ********************/
/* Login / Logout */
function connection_login($action) {
	$try=false;
	session_start();
	$session_started=true;
	// Gets last page
	if(isset($_SESSION["url"])) {
		$url=$_SESSION["url"];
	} else {
		$url="index.php";
	}
	// login / logout
	if($action == "logout") {
		session_start();
		session_destroy();
		header("Location: index.php");
	} else {
		// password test
		if(isset($_POST['login']) && isset($_POST['password'])) {
			$try=true;
			$login = $_POST['login'];
			$password = $_POST['password'];
			libxml_disable_entity_loader(false);
			$xml = simplexml_load_file('data/users.xml');
			$users = $xml->xpath('/conf/users/user');
			$found = false;
			// matching user search
			foreach($users as $user) {
				if($user['login'] == $login && $user['password'] == hash('sha256', hash('sha256', $user['login']).":".$password)) {
					$found = true;
				}
			}
			if($found == true) {
				// good ! We found the Precious >:)
				session_start();
				$_SESSION["login"] = $_REQUEST["login"];
				ini_set("session.cookie_lifetime", "0");
			}
			header("Location: ".$url);
		}
	}
}

/* Sign Up as new user */
function connection_signup() {
?>
<div class="container">
		<div class="row-fluid">
			<div class="span3"></div>
			<form action="connection.php?do=register" method="post" class="span6 block" id="signup-form">
				<p class="text-center"><strong>Formulaire d'inscription - Art-SoftWare.fr</strong></p>
				<input class="span12" type="text" name="login" placeholder="Pseudo" required /><br />
				<input class="span12" type="password" name="password-1" placeholder="Mot de passe" required /><br />
				<input class="span8" type="password" name="password-2" placeholder="Confirm. mot de passe" required />
				<input type="submit" class="btn span3" id="submitsignup" name="submit" value="S'inscrire" />
			</form>
		</div>
</div>
<?php
}

/* Register a new user who just signed up */
function connection_register_user() {
	$username = $_POST['login'];
	$passwd = hash('sha256', hash('sha256', $username).":".$_POST['password-1']);
	$passwd_verif = hash('sha256', hash('sha256', $username).":".$_POST['password-2']);

	if ($passwd !== $passwd_verif) {
		// Password check failed
?>
	<div class="container">
		<div class="row-fluid">
			<div class="span3"></div>
			<div class="span6 block text-center">
				<p>Je suis désolé, mais les mots de passe ne correspondent pas.</p>
				<p><a href="connection.php?do=sign">Vous voudriez peut-être retenter à nouveau ?</a></p>
			</div>
		</div>
	</div>
<?php
	} else {
		// Yay ! We are registered !
		session_destroy();

		libxml_disable_entity_loader(false);
		$xml = simplexml_load_file("data/users.xml");
		$users = $xml->xpath("/conf/users");
		$user = $users[0]->addChild('user');
		$user->addAttribute('login', $username);
		$user->addAttribute('password', $passwd);
		$xml->asXML('data/users.xml');

		session_start();
		$_SESSION["login"] = $login;
		ini_set("session.cookie_lifetime", "0");
?>
	<div class="container">
		<div class="row-fluid">
			<div class="span3"></div>
			<div class="span6 block text-center">
				<p>Vous avez bien été enregistré !</p>
				<p>Vous pouvez dès à présent vous connecter.</p>
				<p>Bonne navigation et à très bientôt !</p>
			</div>
		</div>
	</div>
<?php
	}
}

if ($action !== "login" && $action !== "logout") {
	include("footer.php");
}
?>
